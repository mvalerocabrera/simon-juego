// historial contendra un array con los colores a pulsar
var historial=Array();

// pulsado contendra un array con los valores pulsados por el usuario
var pulsado=Array();

// esta variable define cuando el usuario puede empezar a jugar
var jugando=false;

$(document).ready(function(){

    // Evento que se ejecuta cuando se pulsa el botón para empezar a jugar
    $("input[type=button]").click(function(){

        // inicializamos el array del historial
        historial=Array();

        // Indicamos que inicie un nuevo movimiento
        nuevoMovimiento();

    });

    // Evento que se ejecuta cuando se pulsa sobre uno de los cuadros
    
    $("#simon div").click(function(){

        if(jugando){

            // Añadimos la pulsacion al array pulsado
            pulsado.push($(this).index());

            // Verificamos que nuestra pulsacion sea correcta
            // Esta funcion nos devuelve:
            //  1 Todos los colores pulsados son correctos y hay que mostrar un nuevo color
            //  2 Todos los colores pulsados son correctos pero todavia falta pulsar mas colores
            //  0 Ha habido un error
            verificacion=verificarPulsacion();

            if(verificacion==1){

                // Todo va bien, añadimos un nuevo movimiento
                nuevoMovimiento();
            }

            else if(verificacion==0){

                // El usuario se ha equivocado, mostraremos el error
                mostrarError();
            }
        }
    });
});

/**

    * Esta funcion genera un nuevo color

    */
function nuevoMovimiento(){

    jugando=false;

    $("#mensaje").html("Mostrando los colores...");

    pulsado=Array();

    var nuevoMovimiento=Math.floor(Math.random()*4)

    historial.push(nuevoMovimiento);

    $("#movimientos").html(historial.length);

    mostrarColores(0);
}

/**
 * Esta funcion muestra todos los colores que el usuario tendra que pulsar

    * Recibe el indice del array de colores a mostrar

    */

function mostrarColores(indice){

    $("#simon div").removeClass("encender")

    if(historial[indice]>=0){
        $("#simon div:nth-child("+(historial[indice]+1)+")").addClass("encender");

        setTimeout(function(){ocultarColores(indice+1)},800);

    }else{

        jugando=true;

        $("#mensaje").html("Ya puedes empezar...");
    }
}



/**

    * Esta funcion forma parte del proces de mostrar los colores mostrarColores()

    * Esconde el color

    */

function ocultarColores(indice){

    $("#simon div").removeClass("encender")

    setTimeout(function(){mostrarColores(indice)},500);
}

/**

    * Esta funcion verifica las pulsaciones del usuario

    * Devuelve:

    *  1 Todos los colores pulsados son correctos y hay que mostrar un nuevo color

    *  2 Todos los colores pulsados son correctos pero todavia falta pulsar mas colores

    *  0 Ha habido un error

    */

function verificarPulsacion(){

    for(var i=0;i<historial.length;i++){
        if(pulsado.length>i){

        if(historial[i]!=pulsado[i]){

        return 0;}

        }else{

        return 2;
        }

    }

    if(pulsado.length==historial.length)
        return 1;
        
}

/**

    * Esta funcion muestra los colores que se tenia que haber pulsado y los

    * colores que el usuario ha pulsado

    */

function mostrarError(){

    var colores=["verde", "rojo", "amarillo", "azul"];

    var cadenaColores=" | ";

    var cadenaColoresPulsados=" | ";

    for(var i=0;i<historial.length;i++){
        cadenaColores+=colores[historial[i]]+" | ";
    }

    for(var i=0;i<pulsado.length;i++){
        cadenaColoresPulsados+=colores[pulsado[i]]+" | ";
    }

    $("#mensaje").html("Te has equivocado, puede que seas daltonico, los colores eran:<br>"+cadenaColores+"<br>y tu has pulsado:<br>"+cadenaColoresPulsados);

}

/**

   function beep(audio){

    switch (audio) {
        case 'verde':
            audio1.play();
            break;

        case 'rojo':
            audio2.play();
            break;

        case 'amarillo':
            audio3.play();
            break;

        case 'azul':
            audio4.play();
            break;
    }
}

    */
